import datetime
from collections import defaultdict
from sys import stdin
import itertools


def readline():
    return stdin.readline().rstrip()


def item_pair_hash(b, k, i, j):
    return ((i * k) + j) % b


def read_input():
    N = int(readline())  # number of baskets
    s = float(readline())  # threshold (percentage)
    b = int(readline())  # number of buckets
    return N, s, b, [list(map(int, line.split())) for line in stdin]


def count_items(baskets):
    item_counters = defaultdict(int)
    for basket in baskets:
        for item in basket:
            item_counters[item] += 1
    return item_counters


if __name__ == '__main__':
    # dt = datetime.datetime.now()
    N, s, b, baskets = read_input()
    threshold = s * N
    item_counters = count_items(baskets)
    k = len(item_counters.keys())

    hashes = dict()
    for i, j in itertools.combinations(range(1, k + 1), 2):
        hashes[i, j] = item_pair_hash(b, k, i, j)

    buckets = defaultdict(int)
    for basket in baskets:
        for i, j in itertools.combinations(basket, 2):
            if item_counters[i] >= threshold and item_counters[j] >= threshold:
                buckets[hashes[i, j]] += 1

    # print(datetime.datetime.now() - dt)

    pairs = defaultdict(int)
    for basket in baskets:
        for i, j in itertools.combinations(basket, 2):
            if item_counters[i] >= threshold and item_counters[j] >= threshold and buckets[hashes[i, j]] >= threshold:
                pairs[i, j] += 1

    m = sum([1 if item_counters[i] >= threshold else 0 for i in range(1, k + 1)])
    print(int(m * (m - 1) * 0.5))

    print(len(pairs))

    keys = [key for key in pairs.keys() if pairs[key] >= threshold]
    # keys = list(pairs.keys())
    keys.sort(key=lambda x: pairs[x], reverse=True)
    for key in keys:
        print(pairs[key])

    # print(datetime.datetime.now() - dt)
